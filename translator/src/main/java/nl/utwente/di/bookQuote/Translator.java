package nl.utwente.di.bookQuote;

public class Translator {

    public Double cToF (String celcius){
        double temp = Double.parseDouble(celcius);
        double convTemp = (temp * (9/5))+32;
        return convTemp;
    }
}
